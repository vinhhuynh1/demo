export enum CREERS {
    BODY = 'body',
    CATEGORY = 'category',
    ADDRESS = 'working_address',
    TYPE = 'working_type',
    SALARY_MIN = 'salary_min',
    SALARY_MAX = 'salary_max',
    CURRENCY = 'currency',
    DATE_PUBLISHED = 'date_published',
    IMAGES = 'images',
    TRANSLATION_KEY='translation_key'
  }
export enum PROJECT {
    THUMBNAIL = 'thumbnail',
    PACKAGE = 'package',
    NAME = 'name',
    SERVICE = 'service',
    CLIENT_NAME = 'client_name',
    INTRODUCATION = 'introduction',
    DATE = 'date_published',
    IMAGES = 'images',
    TRANSLATION_KEY='translation_key'
  }

  export enum BLOG {
    THUMBNAIL = 'thumbnail',
    INTRODUCATION = 'introduction',
    DATE = 'date_published',
    IMAGES = 'image',
    BODY = 'body',
    TAGS = 'tags',
    TOTAL_LIKES = 'total_likes',
    SLUG = 'slug',
    TRANSLATION_KEY='translation_key'
  }

  export enum STATUS {
    HIRING = 'Hiring',
    STOP_HIRING = 'Stop Hiring',
    URGENT = 'Urgent',
  }
export enum LINK {
  APP_STORE = 'app_store',
  GOOGLE_STORE = 'google_play',
  BEHANCE = 'behance'
}
export enum STATUS_EMAIL {
  STATUS_TRUE = 201,
  STATUS_FALSE = 400,
}

export enum HEADING {
  HEADING_BLOCK = 'heading_block',
  BLOCK_QUOTE = 'block_quote',
  PARAGRAPH_BLOCK='paragraph_block',
  EMBED_BLOCK='embed_block',
  IMAGE_BLOG='image_block'
}

export enum HEADINGBLOG {
  HEADING_BLOCK = 'heading_block',
  PARAGRAPH_BLOCK='paragraph_block',
  BLOCK_QUOTE = 'block_quote',
  EMBED_BLOCK='embed_block',
  IMAGE_BLOG='image_block'
}

export enum MESSAGE_LOCALE {
  MESSAGE = 'No Page matches the given query.'
}

export enum BLOG_CHECK {
  ALL = 'All'
}

export enum SERVICE_TITLE {
  WEB_DEVELOPMENT = 'WEB DEVELOPMENT',
  UI_UX_DESIGN = 'UI/UX DESIGN',
  GAME_DEVELOPER = 'GAME DEVELOPER',
  QA_TESTER = 'QA/TESTER',
  BA = 'BA',
  DEVOPS = 'DEVOPS',
  CLOUD_SERVER_BASIC = 'CLOUD SERVER BASIC',
  CLOUD_SERVER_STANDAR = 'CLOUD SERVER STANDAR',
  CLOUD_SERVER_PROFESSIONAL = 'CLOUD SERVER PROFESSIONAL',
  CLOUD_SERVER_VIP = 'CLOUD SERVER VIP',
  SOCIAL ="Social & e-Commerce App",
  E_COMMERCE = 'E-COMMERCE & DELIVERY',
  E_Learning = 'E- Learning',
  AR_GAME ='AR GAME',
  MOBA_GAME ='MOBA GAME',
  FARMER_GAME = 'FARMER_GAME',
  PGO = 'PGO',
}

