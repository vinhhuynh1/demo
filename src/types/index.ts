import { ReactHTMLElement } from "react";

export interface Pageable<T> {
  data: T[];
  total: number;
}

export interface IOptions {
  readonly value: string
  readonly label: string
}

export interface Slide {
	key: number,
	content: ReactHTMLElement<HTMLElement>,
	image: string,
	onClick: () => void,
}

export interface CardProps {
  key: number
  content: React.ReactNode
}

export interface Creers {
 items: CreersLIst[]
}

export interface CreersLIst {
  title: string
  working_address:string
  working_type:string
  salary_max:string
  salary_min:string
  status:string
  meta:meta
  category:category
  id:number
  names:string
  tags:tags[]
  name:string
  translate:translate[]
  translation_key:string
 }

 export interface tags {
  name: string
  id:number
 }

export interface translate {
  name: string
  id:number
 }
 export interface category {
  name: string
  id:number
 }

 export interface CreersBanner {
  name: string
  id:number
  meta:meta
  title:string
  introduction:string
  images:images[]
 }

 export interface ProjectBanner {
  name: string
  id:number
  meta:meta
  title:string
  introduction:string
  images:images[]
  value:string
  packages:packages[]
 }

 export interface packages {
  id:string
  name:string
 }

 export interface images {
  value:value
  type:string
 }

 export interface value {
  image_url: string
  title:number
  id:string
 }

 export interface CreersLIstCategori {
  name: string
  id:number
  meta:meta
  category:number
 }

 export interface meta {
  name: string
  download_url:string
  first_published_at:Date
  title:string
 }

 export interface projectList {
  title: string
  working_address:string
  working_type:string
  salary_max:string
  salary_min:string
  status:string
  client_name:string
  item:ListItem[]
  meta:listMeta[]
  id:number
  name:string
  thumbnail:thumbnail[]
  package:Package;
}
interface Package {
  service:Service;
  name:string;
}
interface Service {
  name:string;
}

 interface thumbnail {
  meta:meta[]
}
 interface ListItem {
  client_name: string;
  introduction:string
  date_published:string
}

interface listMeta {
  html_url: string;
}