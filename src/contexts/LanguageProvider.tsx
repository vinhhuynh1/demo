import React from 'react'
import nookies from 'nookies'
import { useRouter } from 'next-translate-routes'
import { usePrevious } from '@/hooks'

interface ILanguageProviderProps {
  children?: React.ReactNode
}

interface LanguageContextInterface {
  language: string
  setLanguage?: (lang: string) => void
  prevLanguage: any
}

const LanguageContext = React.createContext<LanguageContextInterface | null>(null)

function LanguageProvider(props: ILanguageProviderProps) {
  const { children } = props
  const router = useRouter()

  const [language, setLanguage] = React.useState<string>(() => {
    return nookies.get(null)?.NEXT_LOCALE?.toString() || router.locale || 'en'
  })

  const prevLanguage = usePrevious(language)

  const value = {
    language,
    setLanguage,
    prevLanguage,
  }

  return <LanguageContext.Provider value={value}>{children}</LanguageContext.Provider>
}

export const useLanguage = () => React.useContext(LanguageContext) as LanguageContextInterface

export { LanguageContext, LanguageProvider }
