import { Pageable } from '@/types';

export interface TeamResponse {
  created_at: string;
  deleted_at: string;
  facebook: string;
  linkedin: string;
  member_id: number;
  name: string;
  photo: string;
  position: string;
  updated_at: string;
}
export interface TeamState {
  teamsPageable?: TeamResponse[];
}
export interface CareersState {
  id?: string;
  search?:string;
  category?:number;
  tags?:string
  offset?:number;
  limit?:number;
  locale?:string;
  service?:number,
}

export interface UploadCV {
  email_address: string;
  full_name: string;
  phone_number: string;
  portfolio: string;
  upload_your_cv: File;
  status: boolean;
  page: string;
}

export interface BlogDetail {
  id: string;
  title: string;
  date_published: Date;
  introduction: string;
  image: image;
  total_likes: string;
  body: body[];
  tags: tags[];
  translation_key?:string;
}
export interface tags {
  name: string;
}
export interface body {
  type: string;
  value: string;
}

export interface image {
  meta: meta;
}
export interface meta {
  download_url: string;
}

export interface checkLikeState {
  id?: string;
  search?:string;
  category?:number;
  tags?:string
  offset?:number;
  limit?:number;
  client_id?:string;
  translation_key?:string;
}
export interface CheckLike {
  is_liked?: boolean;
}
export interface Message {
  message: string;
}