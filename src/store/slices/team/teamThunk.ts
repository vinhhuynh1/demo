import { instance, instances, noLoginInstance } from '@/services/api/axios';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { CareersState, UploadCV, checkLikeState } from './types';
import { BLOG, CREERS, PROJECT } from '@/types/Enum';

export const fetchTems = createAsyncThunk(
  'team/list',
  async (request: undefined, {rejectWithValue}) => {
    try {
      const response = await instance.get('members');
      return response.data;
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  }
);

export const fetchRecruitment = createAsyncThunk(
  'careers/list',
  async (request: undefined, { rejectWithValue }) => {
    try {
      const response = await instance.get('recruitments');
      return response.data;
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  }
);


export const fetchRecruitments = createAsyncThunk(
  'career.CareerPage',
  async (params: CareersState, { rejectWithValue }) => {
    try {
      const current = params.offset;
      const pageSize = params.limit;
      const locale = params.locale;
      const newParamFilter = params.category ? `${CREERS.BODY},${CREERS.ADDRESS},${CREERS.CATEGORY},${CREERS.TYPE},${CREERS.SALARY_MIN},${CREERS.SALARY_MAX},${CREERS.CURRENCY},${CREERS.DATE_PUBLISHED},${BLOG.TRANSLATION_KEY}&category=${params.category}`:`${CREERS.BODY},${CREERS.ADDRESS},${CREERS.CATEGORY},${CREERS.TYPE},${CREERS.SALARY_MIN},${CREERS.SALARY_MAX},${CREERS.CURRENCY},${CREERS.DATE_PUBLISHED},${BLOG.TRANSLATION_KEY}`;
      const newParam = params.search ? `${CREERS.BODY},${CREERS.ADDRESS},${CREERS.CATEGORY},${CREERS.TYPE},${CREERS.SALARY_MIN},${CREERS.SALARY_MAX},${CREERS.CURRENCY},${CREERS.DATE_PUBLISHED},${BLOG.TRANSLATION_KEY}&search=${params.search}`:`${CREERS.BODY},${CREERS.ADDRESS},${CREERS.CATEGORY},${CREERS.TYPE},${CREERS.SALARY_MIN},${CREERS.SALARY_MAX},${CREERS.CURRENCY},${CREERS.DATE_PUBLISHED},${BLOG.TRANSLATION_KEY}`;
      if(params.category){
        const response = await noLoginInstance.get(`v2/pages?type=career.CareerPage&fields=status,${newParamFilter}&offset=${current}&limit=${pageSize}&locale=${locale}&order=-status`);
        return response.data;
      }else{
        const response = await noLoginInstance.get(`v2/pages?type=career.CareerPage&fields=status,${newParam}&offset=${current}&limit=${pageSize}&locale=${locale}&order=-status`);
        return response.data;
      }
    } catch (error) {
      return rejectWithValue(error.response.data);
    }
  }
);

