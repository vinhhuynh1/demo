// import { MobileContext } from '@/contexts/MobileProvider'
import styles from '@/styles/Navbar.module.css';
import { IOptions } from '@/types';
import classNames from 'classnames';
import Link from 'next/link';
import { useRouter } from 'next/router';
import Arrow from 'public/images/arrow.svg';
import Logo from 'public/images/logofake.png';
import VerticalLine from 'public/images/verticalLine.png';
import * as React from 'react';
import { SingleValue } from 'react-select';
import Drawer, { DrawerRef } from '../../ui/Drawer';
import Image from '../Image';
import LanguageSwitcher from '../LanguageSwitcher';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
const routes = [
  {
    name: 'home',
    pathNames: ['/'],
    path: '/',
  },

  {
    name: 'projects',
    pathNames: ['/projects', '/du-an'],
    path: '/projects',
  },

]

export default function Navbar() {
  // const [currentPosition, setCurrentPosition] = React.useState(10)

  const router = useRouter()
  const { t } = useTranslation('translation')

  // const mobileContext = React.useContext(MobileContext)
  // const isMobile = mobileContext?.isMobile


  const drawerRef = React.useRef<DrawerRef>(null);

  const handleOptionChange = (value: SingleValue<IOptions>) => {
    // console.log(value)
  }

  const [openDialogContactSale, setOpenDialogContactSale] =
    useState<boolean>(false);

  const handleCloseDialogContactSale = () => {
    setOpenDialogContactSale(false);
  };

  // React.useEffect(() => {
  //   if (isOpen && isMobile) document.body.style.overflow = 'hidden'
  //   else document.body.style.overflow = 'unset'
  // }, [isOpen, isMobile])

  return (
    <header
      className={classNames(
        'fixed left-0 right-0 top-0 z-[1000] bg-navy-5 font-montserrat transition-all duration-500 '
      )}
    >
      <div
        className={classNames(
          'container  flex h-14 items-center justify-center md:h-[84px] lg:justify-between sticky'
        )}
      >
        {/* <a href="#" className="group text-sky-600 transition duration-300">
          Link
          
        </a> */}
        <Link href="/" passHref>
          <a className="relative h-[40px] w-[40px] md:h-[40px] md:w-[40px]">
            <Image src={Logo} alt="Vercel Logo" layout="fill" priority />
          </a>
        </Link>

        <div className="">
          <Drawer ref={drawerRef}>
            <ul
              className={`
          ${styles.menu}
          flex flex-col items-start space-x-0 space-y-10 pt-[50px] lg:items-center lg:space-x-[2.25rem] lg:space-y-0 2xl:space-x-8 3xl:space-x-[60px]
        `}
            >
              {routes.map((x) => {
                return (
                  <li
                    onClick={() => {
                      router.push(x.path);
                    }}
                    key={x.path}
                    className={`group relative cursor-pointer font-montserratFontwight6 text-lg font-bold  leading-[24px] tracking-wider transition duration-300 lg:text-base lg:leading-5 xl:text-[18px] xl:leading-[22px] ${
                      router.asPath === x.path ||
                      (x.path !== '/' &&
                        x.pathNames.some((path) =>
                          router.asPath.includes(path)
                        ))
                        ? 'text-active'
                        : 'text-[#FFFFFF]'
                    }`}
                  >
                    {t(x.name)}
                  </li>
                );
              })}
            </ul>
            <div className="">
              <div>
                <LanguageSwitcher />
                <div className="ml-[60px] mt-[-42px]">
                  <Image src={Arrow} alt="verticalLine" />
                </div>
              </div>
            </div>
          </Drawer>
        </div>
        <ul
          className={`
          relative hidden items-start space-x-0 space-y-10 lg:flex lg:items-center lg:space-x-[2.25rem] lg:space-y-0 2xl:space-x-8 3xl:space-x-[60px]
        `}
        >
          {routes.map((x) => {        
            return (
              <li
                onClick={() => {
                  router.push(x.path)
                  // setIsOpen(false)
                }}
                key={x.path}
                className={`group relative cursor-pointer font-montserratFontwight6 text-lg font-bold  leading-[24px] tracking-wider transition duration-300 lg:text-base lg:leading-5 xl:text-[18px] xl:leading-[22px] ${
                  router.asPath === x.path ||
                  (x.path !== '/' &&
                    x.pathNames.some((path) => router.asPath.includes(path)))
                    ? 'text-active'
                    : 'text-[#F8F8F8]'
                }`}
              >
               {t(x.name)}
                <span className="block h-1 max-w-0 bg-active transition-all duration-700 group-hover:max-w-full"></span>
              </li>
            )
          })}
        </ul>
        <div className={`relative flex  ${styles.displayNonePc}`}>      
        <button className={`${styles.button} group relative inline-flex  font-medium transition-all h-[49px] items-center justify-start overflow-hidden rounded-xl bg-[#1de9b6] `}>
          <div className={`${styles.eff}`}></div>
            <a className='w-full text-left font-roboto text-base font-bold text-[#0F103C]' href="#" onClick={() => setOpenDialogContactSale(true)}> Contact sale</a>
        </button>   
         
          <div className="mx-6">
            <Image src={VerticalLine} alt="verticalLine" />
          </div>
          <div className="">
            <div>
              <LanguageSwitcher />
              <div className="ml-[60px] mt-[-42px]">
                <Image src={Arrow} alt="verticalLine" />
              </div>
            </div>
          </div>

          {/* <LanguageSwitcher /> */}
        </div>
      </div>
    </header>
  )
}
