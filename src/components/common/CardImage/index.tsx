import styles from './CardImage.module.css';
import React, { useState } from 'react';
import { useSpring, animated } from '@react-spring/web';
import Image from 'next/image';

function CardImage({ imagen }: { imagen: any }) {
  const [show, setShown] = useState(false);

  const props3 = useSpring({
    borderRadius: '50%',
    transform: show ? 'scale(1.03)' : 'scale(1)',
    boxShadow: show
      ? '0 20px 25px rgb(0 0 0 / 25%)'
      : '0 2px 10px rgb(0 0 0 / 8%)',
  });

  const imgStyle: React.CSSProperties = {
    height: '85%',
    width: '85%',
    position: 'absolute',
    borderRadius: '50%',
    objectFit: 'cover',
  };
  const imgStyle_Tablet: React.CSSProperties = {
    height: '83%',
    width: '83%',
    position: 'absolute',
    borderRadius: '50%',
    objectFit: 'cover',
  };

  return (
    <>
    <div  className={`${styles.card_round_two}`}>
    <animated.div
      className={styles.card}
      style={props3}
      onMouseEnter={() => setShown(true)}
      onMouseLeave={() => setShown(false)}
    >
      <div className={styles.card_round}>
        <div className={styles.card_round_three}>
          <div className={styles.card_round_four}> 
          <></>
          <img src={imagen} alt="card" style={imgStyle} />
          </div>
        </div>
      </div>
    </animated.div>
    </div>
     <div  className={`${styles.card_round_two_table}`}>
    <animated.div
      className={styles.card}
      style={props3}
      onMouseEnter={() => setShown(true)}
      onMouseLeave={() => setShown(false)}
    >
      <div className={styles.card_round}>
        <div className={styles.card_round_three}>
          <div className={styles.card_round_four}> 
          <></>
          <img src={imagen} alt="card" style={imgStyle_Tablet} />
          </div>
        </div>
      </div>
    </animated.div>
    </div>
    </>
  );
}

CardImage.displayName = 'CardImage';

export default CardImage;
