import classNames from 'classnames';
import { AnimatePresence, motion, Transition, Variants } from 'framer-motion';
import React from 'react';
import Portal from '../../common/Portal';

interface DrawerProps {
  containerClassName?: string;
  children?: React.ReactNode;
}

const overlayVariants: Variants = {
  enter: {
    opacity: 1,
  },

  exit: {
    opacity: 0,
  },

  initial: {
    opacity: 0,
  },
};

const drawerVariants: Variants = {
  initial: {
    x: '-100%',
  },

  enter: {
    x: 0,
  },

  exit: {
    x: '-100%',
  },
};

const transition: Transition = {
  ease: 'easeInOut',
};

export type DrawerRef = {
  close: () => void;
  open: () => void;
};

const Drawer = React.forwardRef<DrawerRef, DrawerProps>((props, ref) => {
  const { containerClassName, children } = props;
  const [isOpen, setIsOpen] = React.useState<boolean>(false);
  const handleClose = () => {
    setIsOpen(false);
  };
  const handleOpen = () => setIsOpen(true);

  React.useImperativeHandle(
    ref,
    () => ({
      close: handleClose,
      open: handleOpen,
    }),
    []
  );
  return (
    <div className={classNames(containerClassName)}>
      <div
        className="absolute left-[16px] top-1/2 flex h-[25px] w-[25px] -translate-y-1/2 flex-col justify-between md:left-8 lg:hidden"
        onClick={handleOpen}
      >
        <span className="h-[3.35px] w-[25px] bg-active"></span>
        <span className="h-[3.35px] w-[21px] bg-active"></span>
        <span className="h-[3.35px] w-[25px] bg-active"></span>
      </div>
      <Portal>
        <AnimatePresence exitBeforeEnter>
          {isOpen && (
            <React.Fragment>
              <motion.div
                className={classNames(
                  'fixed inset-0 z-[1000] bg-[rgba(29,233,182,0.05)] backdrop-blur-sm'
                )}
                onClick={handleClose}
                variants={overlayVariants}
                initial="initial"
                animate="enter"
                exit="exit"
                transition={transition}
              />
              <motion.div
                className={classNames(
                  'no-scrollbar fixed left-0 top-0 z-[1000] flex h-full w-[270px] flex-col items-start space-y-10 overflow-y-scroll bg-background pl-[50px]',
                  { 'shadow-[20px_0px_80px_rgba(29,233,182,0.1)]': isOpen }
                )}
                variants={drawerVariants}
                initial="initial"
                animate="enter"
                exit="exit"
                transition={transition}
              >
                {children}
              </motion.div>
            </React.Fragment>
          )}
        </AnimatePresence>
      </Portal>
    </div>
  );
});

Drawer.displayName = 'Drawer';

export default Drawer;
