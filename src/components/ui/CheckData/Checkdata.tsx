
import Image from 'next/image';
import SearchIcon from '@public/common/search-icon-white.png';

export default function CheckData() {
  return (
    <div className='flex justify-center items-center md:h-[100%] h-[200px] 2xl:h-[300px]'>
    <Image
      src={SearchIcon}
      width={24}
      height={24}
      alt="search"
      className="hover:scale-125"
    />
    <p className='ml-2 text-[14px] leading-[17px] text-BodyTextColor lg:text-[18px] lg:leading-6'>{'No data'}</p>
  </div>
  )
}
