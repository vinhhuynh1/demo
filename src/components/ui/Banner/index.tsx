import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import StyleBanner from '@/styles/Banner.module.css';
import BackgroundSlider from 'public/images/backgroudSlider.png';
import EllipseSlider from 'public/images/ellipseSlider-papa.png';
import Image from 'next/image';
import stylesAnimation from '@/styles/animation.module.css';
import BackgroundTransparent from 'public/images/backgroundTransparent.png';
import styled from 'styled-components';
import ImageOffshore from '/public/banner/offshore.png';
import ImageGame from '/public/banner/game.png';
import ImageGameConfig from '/public/banner/game-config.png';
import ImageCloud from '/public/banner/cloud.png';
import ImagePgo from '/public/banner/pgo.png';
import ImageWebApp from '/public/banner/webapp.png';
import ImageHoverWebApp from '/public/banner/hover-webapp.png';
import ImageHoverCloud from '/public/banner/hover-cloud.png';
import ImageHoverGame from '/public/banner/hover-game-ar.png';
import ImageHoverGameConfig from '/public/banner/hover-game-config.png';
import ImageHoverOffshore from '/public/banner/hover-offshore.png';
import ImageHoverPgo from '/public/banner/hover-pgo-life.png';

const Banner = () => {
  const router = useRouter();
  const [value, setValue] = useState<number>(0);
  const [selectedIndex, setSelectedIndex] = useState<number>(0);
  const [hoverItem, setHoverItem] = useState<boolean>(false);
  const [selectedOdc, setSelectedOdc] = useState<boolean>(false);
  const [selectedGameConfig, setSelectedGameConfig] = useState<boolean>(false);
  const [selectedGame, setSelectedGame] = useState<boolean>(false);
  const [selectedWebApp, setSelectedWebApp] = useState<boolean>(false);
  const [selectedCloud, setSelectedCloud] = useState<boolean>(false);
  const [selectedHealTech, setSelectedHealTech] = useState<boolean>(false);

  const secondsAnimation = 8;
  const numberOfElement = 6;

  const objectSize = 120;
  const spacing = 50;
  const overlap = true;
  const offset = 20;
  const d = objectSize + spacing * 2.3;
  const r = objectSize / 2 + spacing / 1;

  useEffect(() => {
    const interval = setInterval(() => {
      setValue((v) => {
        if (v === secondsAnimation) {
          setSelectedIndex((i) => {
            return i === numberOfElement - 1 ? 0 : i + 1;
          });
        }
        return v === secondsAnimation ? 0 : v + 1;
      });
    }, 1000);
    return () => clearInterval(interval);
  });

  const CurvedTextOdc = styled.div`
    position: absolute;
    top: 3%;
    left: 1%;
    z-index: 9999;
    margin-bottom: ${overlap ? `-${r}px` : '0'};
    width: 240px;
    height: 200px;
    path {
      fill: transparent;
    }
    text {
      fill: currentColor;
      text-anchor: middle;
    }

    @media (min-width: 1367px) and (max-width: 1439px) {
      top: 4%;
      left: 2%;
      width: 180px;
    }

    @media (max-width: 1366px) {
      width: 150px;
      top: 4%;
      left: 1%;
    }

    @media (min-width: 1000px) and (max-width: 1023px) {
      top: 3%;
      left: 2%;
      width: 110px;
    }

    @media (max-width: 1000px) {
      top: 3%;
      left: 2%;
      width: 110px;
    }

    @media (max-width: 768px) {
      top: 2%;
      left: 2%;
      width: 110px;
    }

    @media (max-width: 639px) {
      top: 3%;
      left: 3%;
      width: 90px;
    }
  `;

  const CurvedTextOdcHover = styled.div`
    position: absolute;
    top: 4%;
    left: 0%;
    z-index: 9999;
    margin-bottom: ${overlap ? `-${r}px` : '0'};
    width: 190px;
    height: 0px;
    path {
      fill: transparent;
    }
    text {
      fill: currentColor;
      text-anchor: middle;
    }

    @media (min-width: 1440px) {
      top: 4%;
      left: 0%;
      width: 190px;
    }

    @media (max-width: 1439px) {
      top: 4%;
      left: 1%;
      width: 130px;
    }

    @media (max-width: 1360px) {
      top: 3%;
      left: 1%;
      width: 120px;
    }

    @media (max-width: 1000px) {
      top: 2%;
      left: 2%;
      width: 80px;
    }

    @media (max-width: 400px) {
      top: 2%;
      left: 2%;
      width: 55px;
    }
  `;

  const CurvedTextGame = styled.div`
  position: absolute;
  top: 5%;
  left: 1.5%;
  z-index: 9999;
  margin-bottom: ${overlap ? `-${r}px` : '0'};
  width: 240px;
  height: 100px;
  path {
    fill: transparent;
  }
  text {
    fill: currentColor;
    text-anchor: middle;
  }

  @media (min-width: 1367px) and (max-width: 1439px) {
    top: 4%;
    left: -3%;
width: 200px;
    }

    @media (max-width: 1366px) {
      top: 5%;
    left: 0%;
    width: 170px;
  }

  @media (min-width: 1024px) and (max-width: 1100px) {
    top: 4%;
    left: 1%;
    width: 130px;
  }

  @media (min-width: 1000px) and (max-width: 1023px) {
    top: 4%;
    left: 1%;
    width: 110px;
  }

  @media (max-width: 1000px) {
    top: 3%;
    left: 2%;
    width: 110px;
  }

  @media (max-width: 639px) {
    top: 3%;
    left: -2%;
    width: 100px;
  }
  `;

  const CurvedTextGameHover = styled.div`
    position: absolute;
    top: 4%;
    left: 1%;
    z-index: 9999;
    margin-bottom: ${overlap ? `-${r}px` : '0'};
    width: 200px;
    height: 100px;
    path {
      fill: transparent;
    }
    text {
      fill: currentColor;
      text-anchor: middle;
    }

    @media (min-width: 1440px) {
      top: 4%;
      left: 4%;
      width: 160px;
    }

    @media (max-width: 1439px) {
      top: 4%;
      left: 1%;
      width: 130px;
    }

    @media (max-width: 1360px) {
      top: 4%;
      left: 1%;
      width: 120px;
    }

    @media (max-width: 1000px) {
      top: 3%;
      left: 0%;
      width: 90px;
    }

    @media (max-width: 500px) {
      top: 4%;
      left: 6%;
      width: 65px;
    }

    @media (max-width: 400px) {
      top: 3%;
      left: 5%;
      width: 50px;
    }
  `;

  const CurvedTextGameConfig = styled.div`
    position: absolute;
    top: 4%;
    left: 6%;
    z-index: 9999;
    margin-bottom: ${overlap ? `-${r}px` : '0'};
    width: 210px;
    height: 100px;
    path {
      fill: transparent;
    }
    text {
      fill: currentColor;
      text-anchor: middle;
    }

    @media (min-width: 1440px) {
      top: 3%;
      left: 7%;
      width: 210px;
    }

    @media (max-width: 1439px) {
      top: 3%;
      left: 6%;
      width: 165px;
    }

    @media (max-width: 1366px) {
      top: 3%;
      left: 5%;
      width: 170px;
    }

    @media (min-width: 1100px) and (max-width: 1280px) {
      top: 2%;
      left: 6%;
      width: 165px;
    }

    @media (min-width: 1024px) and (max-width: 1100px) {
      top: 4%;
      left: 6%;
      width: 120px;
    }

    @media (min-width: 1000px) and (max-width: 1023px) {
      top: 4%;
      left: 6%;
      width: 110px;
    }

    @media (max-width: 1000px) {
      top: 4%;
      left: 6%;
      width: 110px;
    }

    @media (max-width: 950px) {
      top: 3%;
      left: 7%;
      width: 80px;
    }

    @media (max-width: 840px) {
      top: 3%;
      left: 6%;
      width: 110px;
    }

    @media (min-width: 768px) and (max-width: 840px) {
      top: 4%;
      left: 7%;
      width: 90px;
    }

    @media (max-width: 639px) {
      top: 4%;
      left: 7%;
      width: 90px;
    }
  `;

  const CurvedTextGameConfigHover = styled.div`
    position: absolute;
    top: 2%;
    left: 4%;
    z-index: 9999;
    margin-bottom: ${overlap ? `-${r}px` : '0'};
    width: 190px;
    height: 100px;
    path {
      fill: transparent;
    }
    text {
      fill: currentColor;
      text-anchor: middle;
    }

    @media (min-width: 1440px) {
      top: 3%;
      left: 4%;
      width: 190px;
    }

    @media (max-width: 1439px) {
      top: 4%;
      left: 6%;
      width: 130px;
    }

    @media (max-width: 1360px) {
      top: 4%;
      left: 6%;
      width: 120px;
    }

    @media (max-width: 1280px) {
      top: 4%;
      left: 6%;
      width: 120px;
    }

    @media (max-width: 1024px) {
      top: 4%;
      left: 7%;
      width: 90px;
    }

    @media (max-width: 1000px) {
      top: 4%;
      left: 7%;
      width: 80px;
    }

    @media (max-width: 950px) {
      top: 4%;
      left: 6%;
      width: 65px;
    }

    @media (max-width: 850px) {
      top: 5%;
      left: 8.5%;
      width: 65px;
    }

    @media (max-width: 400px) {
      top: 5%;
      left: 7%;
      width: 65px;
    }

    @media (max-width: 500px) {
      top: 5%;
      left: 6%;
      width: 65px;
    }
  `;

  const CurvedTextWebApp = styled.div`
    position: absolute;
    top: 4%;
    left: 16%;
    z-index: 9999;
    margin-bottom: ${overlap ? `-${r}px` : '0'};
    width: 170px;
    height: 100px;
    path {
      fill: transparent;
    }
    text {
      fill: currentColor;
      text-anchor: middle;
    }

    @media (min-width: 1360px) and (max-width: 1439px) {
      top: 4%;
      left: 15%;
      width: 160px;
    }

    @media (max-width: 1360px) {
      top: 3%;
      left: 10%;
      width: 160px;
    }

    @media (min-width: 1024px) and (max-width: 1100px) {
      top: 4%;
      left: 11%;
      width: 140px;
    }

    @media (min-width: 1000px) and (max-width: 1023px) {
      top: 4%;
      left: 11%;
      width: 90px;
    }

    @media (max-width: 1000px) {
      top: 2%;
      left: 15%;
      width: 80px;
    }

    @media (max-width: 950px) {
      top: 3%;
      left: 15%;
      width: 80px;
    }

    @media (max-width: 840px) {
      top: 3%;
      left: 15%;
      width: 80px;
    }

    @media (max-width: 768px) {
      top: 4%;
      left: 15%;
      width: 80px;
    }

    @media (max-width: 767px) {
      top: 4%;
      left: 15%;
      width: 100px;
    }

    @media (max-width: 400px) {
      top: 4%;
      left: 15%;
      width: 80px;
    }
  `;

  const CurvedTextWebAppHover = styled.div`
    position: absolute;
    top: 3%;
    left: 13%;
    z-index: 9999;
    margin-bottom: ${overlap ? `-${r}px` : '0'};
    width: 170px;
    height: 100px;
    path {
      fill: transparent;
    }
    text {
      fill: currentColor;
      text-anchor: middle;
    }

    @media (max-width: 1439px) {
      top: 4%;
      left: 12%;
      width: 120px;
    }

    @media (max-width: 1360px) {
      top: 4%;
      left: 13%;
      width: 125px;
    }

    @media (max-width: 1280px) {
      top: 4%;
      left: 12%;
      width: 100px;
    }

    @media (max-width: 1000px) {
      top: 4%;
      left: 12%;
      width: 80px;
    }

    @media (max-width: 950px) {
      top: 4%;
      left: 12%;
      width: 70px;
    }

    @media (max-width: 850px) {
      top: 3%;
      left: 16%;
      width: 70px;
    }

    @media (max-width: 500px) {
      top: 3%;
      left: 14%;
      width: 60px;
    }
  `;

  const CurvedTextHealTech = styled.div`
  position: absolute;
  top: 4%;
  left: -1%;
  z-index: 9999;
  margin-bottom: ${overlap ? `-${r}px` : '0'};
  width: 250px;
  height: 100px;
  path {
    fill: transparent;
  }
  text {
    fill: currentColor;
    text-anchor: middle;
  }

  @media (max-width: 1439px) {
    top: 13%;
    left: 0%;
    width: 200px;
  }

  @media (min-width: 1360px) and (max-width: 1400px) {
    top: 14%;
    left: 1%;
    width: 140px;
    }

    @media (max-width: 1360px) {
      top: 14%;
      left: 0%;
      width: 140px;
  }

  @media (min-width: 1000px) and (max-width: 1025px) {
    top: 5%;
    left: -2%;
    width: 140px;
  }

  @media (max-width: 1000px) {
    top: 14%;
    left: 1%;
    width: 120px;
  }

  @media (max-width: 639px) {
    top: 14%;
    left: 1%;
    width: 100px;
  }
  `;

  const CurvedTextHealTechHover = styled.div`
    position: absolute;
    top: 13%;
    left: 0%;
    z-index: 9999;
    margin-bottom: ${overlap ? `-${r}px` : '0'};
    width: 220px;
    height: 100px;
    path {
      fill: transparent;
    }
    text {
      fill: currentColor;
      text-anchor: middle;
    }

    @media (min-width: 1440px) {
      top: 5%;
      left: -3%;
      width: 180px;
    }

    @media (max-width: 1439px) {
      top: 14%;
      left: -3%;
      width: 150px;
    }

    @media (min-width: 1360px) and (max-width: 1400px) {
      top: 15%;
      left: 2%;
      width: 125px;
    }

    @media (max-width: 1359px) {
      top: 15%;
      left: -1%;
      width: 130px;
    }

    @media (max-width: 1000px) {
      top: 14%;
      left: 0%;
      width: 90px;
    }

    @media (max-width: 500px) {
      top: 14%;
      left: 0%;
      width: 70px;
    }
  `;

  const CurvedTextCloud = styled.div`
    position: absolute;
    top: 9%;
    left: 4%;
    z-index: 9999;
    margin-bottom: ${overlap ? `-${r}px` : '0'};
    width: 240px;
    height: 100px;
    path {
      fill: transparent;
    }
    text {
      fill: currentColor;
      text-anchor: middle;
    }

    @media (max-width: 1439px) {
      top: 9%;
      left: 9%;
      width: 160px;
    }

    @media (min-width: 1360px) and (max-width: 1400px) {
      top: 8%;
      left: 6%;
      width: 140px;
    }

    @media (max-width: 1360px) {
      top: 8%;
      left: 6%;
      width: 140px;
    }

    @media (min-width: 1000px) and (max-width: 1023px) {
      top: 8%;
      left: 6%;
      width: 120px;
    }

    @media (max-width: 1000px) {
      top: 8%;
      left: 4%;
      width: 110px;
    }

    @media (max-width: 639px) {
      top: 8%;
      left: 4%;
      width: 90px;
    }
  `;

  const CurvedTextCloudHover = styled.div`
    position: absolute;
    top: 9%;
    left: 8%;
    z-index: 9999;
    margin-bottom: ${overlap ? `-${r}px` : '0'};
    width: 180px;
    height: 100px;
    path {
      fill: transparent;
    }
    text {
      fill: currentColor;
      text-anchor: middle;
    }

    @media (min-width: 1440px) {
      top: 9%;
      left: 8%;
      width: 180px;
    }

    @media (max-width: 1439px) {
      top: 8%;
      left: 2%;
      width: 140px;
    }

    @media (max-width: 1360px) {
      top: 8%;
      left: 4%;
      width: 120px;
    }

    @media (max-width: 1000px) {
      top: 8%;
      left: 4%;
      width: 85px;
    }

    @media (max-width: 500px) {
      top: 8%;
      left: 4%;
      width: 70px;
    }
  `;

  return (
    <div
      className="relative min-h-[200px]"
      style={{
        backgroundImage: `url(${BackgroundSlider.src})`,
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
      }}
    >
      <div
        className="flex flex-row 
        md:px-[32px]
        lg:px-[32px]
        xl:px-10
        2xl:px-[10%] pt-[6%]"
      >
        <div
          className={`${StyleBanner.contentBanner} md:pb-[200px] md:pt-[100px] lg:py-[350px] xl:py-[350px] 2xl:pb-[400px] 2xl:pt-[300px]`}
        >
          <div
            className="font-bold text-[#1DE9B6]
            md:text-[24px] md:leading-[30px] 
            xl:text-[32px] xl:leading-[40px]
            2xl:text-[64px] 2xl:leading-[80px]
          "
          >
            <p className="font-montserrat">Patience</p>
            <p className="font-montserrat">on the path</p>
          </div>
          <div
            className="mt-[36px] text-base leading-6 tracking-[0.2em] text-white
              md:mr-[36px] md:mt-[20px] md:text-[12px]
              xl:text-[14px] xl:leading-[17px]
              2xl:text-[16px] 2xl:leading-[24px]
          "
          >
            <p className="font-montserratRegular">
              <span className="font-montserratRegular tracking-[8px]">
                This
              </span>
              <span className="font-montserratRegular tracking-[.05em]">
                is a technology company on cross-platform application for both
                outsourcing and production by delivering software applications
                contributing to build the life to be better.
              </span>
            </p>
          </div>
        </div>
        <div className="flex w-full items-center justify-center mt-[5%] sm:mt-0">
          <div
            className={`relative ${StyleBanner.imageBanner}
              sm:mb-[100px] sm:mt-[130px]
              sm:h-[250px] sm:w-[250px]
              md:h-[300px] md:w-[300px]
              lg:h-[400px] lg:w-[400px]
              xl:h-[450px] xl:w-[450px]
              2xl:h-[550px] 2xl:w-[550px]
            `}
            style={{
              backgroundImage: `url(${EllipseSlider.src})`,
              backgroundSize: 'cover',
              backgroundRepeat: 'no-repeat',
              backgroundPosition: 'center',
            }}
          >
            {selectedIndex === 0 && !hoverItem ? (
              <div>
                <div
                  className={`${stylesAnimation.zoomIn} absolute hover:cursor-pointer 
                  sm:left-[-85px] sm:top-[-50px] sm:h-[120px] sm:w-[120px]
                  md:left-[-80px] md:top-[-40px] md:h-[120px] md:w-[120px]
                  lg:left-[-100px] lg:top-[-40px] lg:h-[160px] lg:w-[160px]
                  xl:left-[-160px] xl:top-[-40px] xl:h-[200px] xl:w-[200px]
                  2xl:left-[-190px] 2xl:top-[-60px] 2xl:h-[260px] 2xl:w-[260px]
                  ${StyleBanner.positionAnimationOdc}
                  `}
                  onClick={() => router.push('/services/offshore')}
                >
                  <Image
                    layout="fill"
                    src={ImageHoverOffshore}
                    alt="introduce"
                  />
                  <CurvedTextOdc className="curved-text">
                    <svg viewBox={`0 0 ${d + offset * 2} ${r + offset * 2}`}>
                      <path
                        id="curve"
                        d={`M${offset},${r + offset} A${r},${r} 0 0,1 ${
                          d + offset
                        },${r + offset}`}
                      />
                      <text>
                        <textPath
                          xlinkHref="#curve"
                          startOffset="50%"
                          fontSize={14}
                        >
                          {'OFFSHORE DEVELOPMENT CENTER'}
                        </textPath>
                      </text>
                    </svg>
                  </CurvedTextOdc>
                </div>
                <div
                  className={`absolute ${stylesAnimation.zoomOut}
                  sm:left-[0px] sm:top-[30px] sm:h-[60px] sm:w-[60px]
                  md:left-[10px] md:top-[40px] md:h-[60px] md:w-[60px]
                  lg:left-[10px] lg:top-[60px] lg:h-[80px] lg:w-[80px]
                  xl:left-[-10px] xl:top-[90px] xl:h-[100px] xl:w-[100px]
                  2xl:h-[130px] 2xl:w-[130px]
                  ${StyleBanner.animationOdc}
                  `}
                >
                  <Image src={ImageOffshore} layout="fill" alt="introduce" />
                </div>
              </div>
            ) : (
              <div
                className={`absolute hover:cursor-pointer 
                  ${StyleBanner.animationOdc}
                  sm:left-[0px] sm:top-[30px] sm:h-[60px] sm:w-[60px]
                  md:left-[10px] md:top-[40px] md:h-[60px] md:w-[60px]
                  lg:left-[10px] lg:top-[60px] lg:h-[80px] lg:w-[80px]
                  xl:left-[-10px] xl:top-[90px] xl:h-[100px] xl:w-[100px]
                  2xl:h-[130px] 2xl:w-[130px]
              `}
                onClick={() => router.push('/services/offshore')}
                onMouseOver={() => {
                  setHoverItem(true);
                  setSelectedOdc(true);
                }}
                onMouseOut={() => {
                  setHoverItem(false);
                  setSelectedOdc(false);
                }}
              >
                <Image src={ImageOffshore} layout="fill" alt="introduce" />
                {selectedOdc && (
                  <div
                    className={`absolute hover:cursor-pointer ${stylesAnimation.hover}
                      xm:left-[-40px] xm:top-[-45px] xm:h-[60px] xm:w-[60px]
                      sd:left-[-85px] sd:top-[-50px] sd:h-[90px] sd:w-[90px]
                      md:left-[-70px] md:top-[-70px] md:h-[90px] md:w-[90px]
                      lg:left-[-100px] lg:top-[-120px] lg:h-[130px] lg:w-[130px]
                      xl:left-[-100px] xl:top-[-120px] xl:h-[140px] xl:w-[140px]
                      2xl:left-[-180px] 2xl:top-[-170px] 2xl:h-[200px] 2xl:w-[200px]
                    `}
                    onClick={() => router.push('/services/offshore')}
                  >
                    <CurvedTextOdcHover className="curved-text">
                      <svg viewBox={`0 0 ${d + offset * 2} ${r + offset * 2}`}>
                        <path
                          id="curve"
                          d={`M${offset},${r + offset} A${r},${r} 0 0,1 ${
                            d + offset
                          },${r + offset}`}
                        />
                        <text>
                          <textPath
                            xlinkHref="#curve"
                            startOffset="50%"
                            fontSize={14}
                          >
                            {'OFFSHORE DEVELOPMENT CENTER'}
                          </textPath>
                        </text>
                      </svg>
                    </CurvedTextOdcHover>
                    <Image
                      width={200}
                      height={200}
                      src={ImageHoverOffshore}
                      alt="introduce"
                    />
                  </div>
                )}
              </div>
            )}

            {selectedIndex === 1 && !hoverItem ? (
              <div>
                <div
                  className={`${stylesAnimation.zoomIn} absolute hover:cursor-pointer 
                  sm:left-[60px] sm:top-[-110px] sm:h-[120px] sm:w-[120px]
                  md:left-[90px] md:top-[-110px] md:h-[120px] md:w-[120px]
                  lg:left-[130px] lg:top-[-120px] lg:h-[140px] lg:w-[140px]
                  ls:left-[110px] ls:top-[-160px] ls:h-[180px] ls:w-[180px]
                  xl:left-[130px] xl:top-[-180px] xl:h-[200px] xl:w-[200px]
                  2xl:left-[90px] 2xl:top-[-230px] 2xl:h-[260px] 2xl:w-[260px]
                  ${StyleBanner.positionAnimationGame}
                  `} 
                  onClick={() => router.push('/services/gameservice')}
                >
                  <Image
                    layout="fill"
                    src={ImageHoverGame}
                    alt="introduce"
                  />
                  <CurvedTextGame className="curved-text">
                    <svg viewBox={`0 0 ${d + offset * 2} ${r + offset * 2}`}>
                      <path
                        id="curve"
                        d={`M${offset},${r + offset} A${r},${r} 0 0,1 ${
                          d + offset
                        },${r + offset}`}
                      />
                      <text>
                        <textPath
                          xlinkHref="#curve"
                          startOffset="52%"
                          fontSize={14}
                        >
                          {'2D/3D/AR/VR GAME'}
                        </textPath>
                      </text>
                    </svg>
                  </CurvedTextGame>
                </div>
                                <div
                  className={`absolute ${stylesAnimation.zoomOut}
                    ${StyleBanner.animationGame}
                    sm:left-[90px] sm:top-[-20px] sm:h-[60px] sm:w-[60px]
                    md:left-[120px] md:top-[-20px] md:h-[60px] md:w-[60px]
                    lg:left-[160px] lg:top-[-25px] lg:h-[80px] lg:w-[80px]
                    xl:left-[175px] xl:top-[-40px] xl:h-[100px] xl:w-[100px]
                    2xl:left-[210px] 2xl:top-[-50px] 2xl:h-[130px] 2xl:w-[130px]
                  `}
                >
                  <Image layout="fill" src={ImageGame} alt="introduce" />
                </div>
              </div>
            ) : (
              <div
                className={`absolute hover:cursor-pointer
                sm:left-[90px] sm:top-[-20px] sm:h-[60px] sm:w-[60px]
                md:left-[120px] md:top-[-20px] md:h-[60px] md:w-[60px]
                lg:left-[160px] lg:top-[-25px] lg:h-[80px] lg:w-[80px]
                xl:left-[175px] xl:top-[-40px] xl:h-[100px] xl:w-[100px]
                2xl:left-[210px] 2xl:top-[-50px] 2xl:h-[130px] 2xl:w-[130px]
                ${StyleBanner.animationGame}
                `}
                onClick={() => router.push('/services/gameservice')}
                onMouseOver={() => {
                  setHoverItem(true);
                  setSelectedGame(true);
                }}
                onMouseOut={() => {
                  setHoverItem(false);
                  setSelectedGame(false);
                }}
              >
                <Image layout="fill" src={ImageGame} alt="introduce" />
                {selectedGame && (
                <div
                  className={`absolute hover:cursor-pointer ${stylesAnimation.hover}
                      xm:left-[5px] xm:top-[-60px] xm:h-[60px] xm:w-[60px]
                  sd:left-[-10px] sd:top-[-75px] sd:h-[80px] sd:w-[80px]
                      md:left-[-15px] md:top-[-90px] md:h-[90px] md:w-[90px]
                  lg:left-[-25px] lg:top-[-130px] lg:h-[130px] lg:w-[130px]
                  xl:left-[-15px] xl:top-[-140px] xl:h-[140px] xl:w-[140px]
                  2xl:left-[-110px] 2xl:top-[-160px] 2xl:h-[180px] 2xl:w-[180px]
                                    `}
                  onClick={() => router.push('/services/gameservice')}
                >
                  <Image
                    layout="fill"
                    src={ImageHoverGame}
                    alt="introduce"
                  />
                  <CurvedTextGameHover className="curved-text">
                    <svg viewBox={`0 0 ${d + offset * 2} ${r + offset * 2}`}>
                      <path
                        id="curve"
                        d={`M${offset},${r + offset} A${r},${r} 0 0,1 ${
                          d + offset
                        },${r + offset}`}
                      />
                      <text>
                        <textPath
                          xlinkHref="#curve"
                          startOffset="50%"
                          fontSize={14}
                        >
                          {'2D/3D/AR/VR GAME'}
                        </textPath>
                      </text>
                    </svg>
                  </CurvedTextGameHover>
                </div>
                )}
              </div>
            )}

            {selectedIndex === 2 && !hoverItem ? (
              <div>
                <div
                  className={`${stylesAnimation.zoomIn} absolute hover:cursor-pointer 
                    ${StyleBanner.positionAnimationGameConfig}
                    xm:right-[-40px] xm:top-[-40px] xm:h-[90px] xm:w-[90px]
                    sd:right-[-90px] sd:top-[-40px] sd:h-[120px] sd:w-[120px]
                    sm:right-[-90px] sm:top-[-40px] sm:h-[120px] sm:w-[120px]
                    md:right-[-30px] md:top-[-20px] md:h-[100px] md:w-[100px]
                    ms:right-[-40px] ms:top-[-30px] ms:h-[120px] ms:w-[120px]                    
                    lg:right-[-60px] lg:top-[-35px] lg:h-[130px] lg:w-[130px]
                    ls:right-[-100px] ls:top-[-50px] ls:h-[160px] ls:w-[180px]
                    xl:right-[-140px] xl:top-[-30px] xl:h-[160px] xl:w-[180px]
                    2xl:right-[-172px] 2xl:top-[-30px] 2xl:h-[220px] 2xl:w-[230px]
                  `}
                  onClick={() => router.push('/services/gameconfig')}
                >
                  <Image
                    layout="fill"
                    src={ImageHoverGameConfig}
                    alt="introduce"
                  />
                  <CurvedTextGameConfig className="curved-text">
                    <svg viewBox={`0 0 ${d + offset * 2} ${r + offset * 2}`}>
                      <path
                        id="curve"
                        d={`M${offset},${r + offset} A${r},${r} 0 0,1 ${
                          d + offset
                        },${r + offset}`}
                      />
                      <text>
                        <textPath
                          xlinkHref="#curve"
                          startOffset="50%"
                          fontSize={15}
                        >
                          {'GAMIFICATION MARKETING FLATFORM'}
                        </textPath>
                      </text>
                    </svg>
                  </CurvedTextGameConfig>
                </div>
                <div
                  className={`absolute ${stylesAnimation.zoomOut}
                    ${StyleBanner.animationGameConfig}
                    sm:right-[5px] sm:top-[25px] sm:h-[60px] sm:w-[60px]
                    md:right-[5px] md:top-[40px] md:h-[60px] md:w-[60px]
                    lg:right-[0px] lg:top-[60px] lg:h-[80px] lg:w-[80px]
                    xl:right-[-20px] xl:top-[90px] xl:h-[100px] xl:w-[100px]
                    2xl:right-[-30px] 2xl:top-[90px] 2xl:h-[130px] 2xl:w-[130px]
                  `}
                >
                  <Image layout="fill" src={ImageGameConfig} alt="introduce" />
                </div>
              </div>
            ) : (
              <div
                className={`absolute hover:cursor-pointer
                  ${StyleBanner.animationGameConfig}
                  sm:right-[5px] sm:top-[25px] sm:h-[60px] sm:w-[60px]
                  md:right-[5px] md:top-[40px] md:h-[60px] md:w-[60px]
                  lg:right-[0px] lg:top-[60px] lg:h-[80px] lg:w-[80px]
                  xl:right-[-20px] xl:top-[90px] xl:h-[100px] xl:w-[100px]
                  2xl:right-[-30px] 2xl:top-[90px] 2xl:h-[130px] 2xl:w-[130px]
                `}
                onClick={() => router.push('/services/gameconfig')}
                onMouseOver={() => {
                  setHoverItem(true);
                  setSelectedGameConfig(true);
                }}
                onMouseOut={() => {
                  setHoverItem(false);
                  setSelectedGameConfig(false);
                }}
              >
                <Image layout="fill" src={ImageGameConfig} alt="introduce" />
                {selectedGameConfig && (
                  <div
                    className={`absolute hover:cursor-pointer ${stylesAnimation.hover}
                      xm:right-[-30px] xm:top-[-60px] xm:h-[70px] xm:w-[70px]
                      sd:right-[-30px] sd:top-[-60px] sd:h-[70px] sd:w-[70px]
                      md:right-[-35px] md:top-[-65px] md:h-[75px] md:w-[75px]
                      ms:right-[-35px] ms:top-[-60px] ms:h-[70px] ms:w-[70px]
                      lg:right-[-60px] lg:top-[-90px] lg:h-[100px] lg:w-[100px]
                      lx:right-[-120px] lx:top-[-100px] lx:h-[130px] lx:w-[130px]
                      xl:right-[-120px] xl:top-[-100px] xl:h-[140px] xl:w-[140px]
                      2xl:right-[-110px] 2xl:top-[-160px] 2xl:h-[177px] 2xl:w-[200px]
                      3xl:right-[-170px] 3xl:top-[-130px] 3xl:h-[177px] 3xl:w-[200px]
                    `}
                    onClick={() => router.push('/services/gameconfig')}
                  >
                    <Image
                      layout="fill"
                      src={ImageHoverGameConfig}
                      alt="introduce"
                    />
                    <CurvedTextGameConfigHover className="curved-text">
                      <svg viewBox={`0 0 ${d + offset * 2} ${r + offset * 2}`}>
                        <path
                          id="curve"
                          d={`M${offset},${r + offset} A${r},${r} 0 0,1 ${
                            d + offset
                          },${r + offset}`}
                        />
                        <text>
                          <textPath
                            xlinkHref="#curve"
                            startOffset="52%"
                            fontSize={14}
                          >
                            {'GAMIFICATION MARKETING PLATFORM'}
                          </textPath>
                        </text>
                      </svg>
                    </CurvedTextGameConfigHover>
                  </div>
                )}
              </div>
            )}

            {selectedIndex === 3 && !hoverItem ? (
              <div>
                <div
                  className={`${stylesAnimation.zoomIn} absolute hover:cursor-pointer 
                    
                    xm:bottom-[-20px] xm:right-[-50px] xm:h-[90px] xm:w-[100px]
                    sd:bottom-[-40px] sd:right-[-90px] sd:h-[120px] sd:w-[130px]
                    sm:bottom-[-40px] sm:right-[-90px] sm:h-[120px] sm:w-[130px]
                    md:bottom-[-20px] md:right-[-35px] md:h-[90px] md:w-[100px]
                    ms:bottom-[-20px] ms:right-[-50px] ms:h-[90px] ms:w-[100px]
                    lg:bottom-[-20px] lg:right-[-80px] lg:h-[140px] lg:w-[160px]
                    ls:bottom-[-30px] ls:right-[-105px] ls:h-[160px] ls:w-[180px]
                    xl:bottom-[-50px] xl:right-[-150px] xl:h-[180px] xl:w-[200px]
                    2xl:bottom-[-40px] 2xl:right-[-160px] 2xl:h-[200px] 2xl:w-[220px]
                  `}
                  onClick={() => router.push('/services/webappsystem')}
                >
                  <Image layout="fill" src={ImageHoverWebApp} alt="introduce" />
                  <CurvedTextWebApp className="curved-text">
                    <svg viewBox={`0 0 ${d + offset * 2} ${r + offset * 2}`}>
                      <path
                        id="curve"
                        d={`M${offset},${r + offset} A${r},${r} 0 0,1 ${
                          d + offset
                        },${r + offset}`}
                      />
                      <text>
                        <textPath
                          xlinkHref="#curve"
                          startOffset="50%"
                          fontSize={16}
                        >
                          {'WEB & APP SYSTEM'}
                        </textPath>
                      </text>
                    </svg>
                  </CurvedTextWebApp>
                </div>
                <div
                  className={`absolute ${stylesAnimation.zoomOut}
                    ${StyleBanner.animationWebApp}
                    sm:bottom-[30px] sm:right-[0px] sm:h-[60px] sm:w-[60px]
                    md:bottom-[30px] md:right-[10px] md:h-[60px] md:w-[60px]
                    lg:bottom-[60px] lg:right-[0px] lg:h-[80px] lg:w-[80px]
                    xl:bottom-[90px] xl:right-[-20px] xl:h-[100px] xl:w-[100px]
                    2xl:bottom-[70px] 2xl:right-[-20px] 2xl:h-[130px] 2xl:w-[130px]
                  `}
                >
                  <Image layout="fill" src={ImageWebApp} alt="introduce" />
                </div>
              </div>
            ) : (
              <div
                className={`absolute hover:cursor-pointer
                  ${StyleBanner.animationWebApp}
                  sm:bottom-[30px] sm:right-[0px] sm:h-[60px] sm:w-[60px]
                  md:bottom-[30px] md:right-[10px] md:h-[60px] md:w-[60px]
                  lg:bottom-[60px] lg:right-[0px] lg:h-[80px] lg:w-[80px]
                  xl:bottom-[90px] xl:right-[-20px] xl:h-[100px] xl:w-[100px]
                  2xl:bottom-[70px] 2xl:right-[-20px] 2xl:h-[130px] 2xl:w-[130px]
                `}
                onClick={() => router.push('/services/webappsystem')}
                onMouseOver={() => {
                  setHoverItem(true);
                  setSelectedWebApp(true);
                }}
                onMouseOut={() => {
                  setHoverItem(false);
                  setSelectedWebApp(false);
                }}
              >
                <Image
                  width={130}
                  height={130}
                  src={ImageWebApp}
                  alt="introduce"
                />
                {selectedWebApp && (
                  <div
                    className={`absolute hover:cursor-pointer ${stylesAnimation.hover}
                      xm:bottom-[-65px] xm:right-[-40px] xm:h-[70px] xm:w-[70px]
                      sd:bottom-[-60px] sd:right-[-40px] sd:h-[70px] sd:w-[70px]
                      md:bottom-[-80px] md:right-[-40px] md:h-[90px] md:w-[90px]
                      ms:bottom-[-65px] ms:right-[-40px] ms:h-[80px] ms:w-[80px]
                      lg:bottom-[-110px] lg:right-[-50px] lg:h-[140px] lg:w-[120px]
                      lx:bottom-[-110px] lx:right-[-110px] lx:h-[130px] lx:w-[150px]
                      xl:bottom-[-120px] xl:right-[-110px] xl:h-[140px] xl:w-[180px]
                      2xl:bottom-[-170px] 2xl:right-[-120px] 2xl:h-[180px] 2xl:w-[200px]
                      3xl:bottom-[-150px] 3xl:right-[-150px] 3xl:h-[180px] 3xl:w-[200px]
                    `}
                    onClick={() => router.push('/services/gameconfig')}
                  >
                    <Image
                      src={ImageHoverWebApp}
                      layout="fill"
                      alt="introduce"
                    />
                    <CurvedTextWebAppHover className="curved-text">
                      <svg viewBox={`0 0 ${d + offset * 2} ${r + offset * 2}`}>
                        <path
                          id="curve"
                          d={`M${offset},${r + offset} A${r},${r} 0 0,1 ${
                            d + offset
                          },${r + offset}`}
                        />
                        <text>
                          <textPath
                            xlinkHref="#curve"
                            startOffset="50%"
                            fontSize={14}
                          >
                            {'WEB & APP SYSTEM'}
                          </textPath>
                        </text>
                      </svg>
                    </CurvedTextWebAppHover>
                  </div>
                )}
              </div>
            )}

            {selectedIndex === 4 && !hoverItem ? (
              <div>
                <div
                  className={`${stylesAnimation.zoomIn} absolute hover:cursor-pointer 
                    ${StyleBanner.positionAnimationHealTech}
                    sm:bottom-[-110px] sm:right-[65px] sm:h-[130px] sm:w-[120px]
                    md:bottom-[-110px] md:right-[90px] md:h-[130px] md:w-[120px]
                    lg:bottom-[-130px] lg:right-[130px] lg:h-[150px] lg:w-[140px]
                    xl:bottom-[-190px] xl:right-[130px] xl:h-[200px] xl:w-[200px]
                    2xl:bottom-[-255px] 2xl:right-[140px] 2xl:h-[260px] 2xl:w-[260px]
                  `}
                  onClick={() => router.push('/services/healthtech')}
                >
                  <Image layout="fill" src={ImageHoverPgo} alt="introduce" />
                  <CurvedTextHealTech className="curved-text">
                    <svg viewBox={`0 0 ${d + offset * 2} ${r + offset * 2}`}>
                      <path
                        id="curve"
                        d={`M${offset},${r + offset} A${r},${r} 0 0,1 ${
                          d + offset
                        },${r + offset}`}
                      />
                      <text>
                        <textPath
                          xlinkHref="#curve"
                          startOffset="50%"
                          fontSize={14}
                        >
                          {'HEALTH TECH PLATFORM'}
                        </textPath>
                      </text>
                    </svg>
                  </CurvedTextHealTech>
                </div>
                <div
                  className={`absolute ${stylesAnimation.zoomOut}
                    ${StyleBanner.animationHealTech}
                    sm:bottom-[-20px] sm:right-[95px] sm:h-[60px] sm:w-[60px]
                    md:bottom-[-20px] md:right-[120px] md:h-[60px] md:w-[60px]
                    lg:bottom-[-25px] lg:right-[160px] lg:h-[80px] lg:w-[80px]
                    xl:bottom-[-40px] xl:right-[180px] xl:h-[100px] xl:w-[100px]
                    2xl:bottom-[-60px] 2xl:right-[210px] 2xl:h-[130px] 2xl:w-[130px]
                  `}
                >
                  <Image layout="fill" src={ImagePgo} alt="introduce" />
                </div>
              </div>
            ) : (
              <div
                className={`absolute hover:cursor-pointer 
                  ${StyleBanner.animationHealTech}
                  sm:bottom-[-20px] sm:right-[95px] sm:h-[60px] sm:w-[60px]
                  md:bottom-[-20px] md:right-[120px] md:h-[60px] md:w-[60px]
                  lg:bottom-[-25px] lg:right-[160px] lg:h-[80px] lg:w-[80px]
                  xl:bottom-[-40px] xl:right-[180px] xl:h-[100px] xl:w-[100px]
                  2xl:bottom-[-60px] 2xl:right-[210px] 2xl:h-[130px] 2xl:w-[130px]
              `}
                onClick={() => router.push('/services/healthtech')}
                onMouseOver={() => {
                  setHoverItem(true);
                  setSelectedHealTech(true);
                }}
                onMouseOut={() => {
                  setHoverItem(false);
                  setSelectedHealTech(false);
                }}
              >
                <Image
                  width={130}
                  height={130}
                  src={ImagePgo}
                  alt="introduce"
                />
                {selectedHealTech && (
                  <div
                    className={`absolute hover:cursor-pointer ${stylesAnimation.hover}
                      xm:bottom-[-70px] xm:right-[-5px] xm:h-[70px] xm:w-[70px]
                      sd:bottom-[-70px] sd:right-[-5px] sd:h-[70px] sd:w-[70px]
                      md:bottom-[-90px] md:right-[-15px] md:h-[90px] md:w-[90px]
                      lg:bottom-[-150px] lg:right-[-25px] lg:h-[150px] lg:w-[130px]
                      xl:bottom-[-145px] xl:right-[-20px] xl:h-[140px] xl:w-[140px]
                      2xl:bottom-[-180px] 2xl:right-[-130px] 2xl:h-[180px] 2xl:w-[180px]
                    `}
                    onClick={() => router.push('/services/gameconfig')}
                  >
                      <Image
                      src={ImageHoverPgo}
                      alt="introduce"
                    />
                    <CurvedTextHealTechHover className="curved-text">
                      <svg viewBox={`0 0 ${d + offset * 2} ${r + offset * 2}`}>
                        <path
                          id="curve"
                          d={`M${offset},${r + offset} A${r},${r} 0 0,1 ${
                            d + offset
                          },${r + offset}`}
                        />
                        <text>
                          <textPath
                            xlinkHref="#curve"
                            startOffset="50%"
                            fontSize={14}
                          >
                            {'HEALTH TECH PLATFORM'}
                          </textPath>
                        </text>
                      </svg>
                    </CurvedTextHealTechHover>
                  
                  </div>
                )}
              </div>
            )}
            {selectedIndex === 5 && !hoverItem ? (
              <div>
                <div
                  className={`${stylesAnimation.zoomIn} absolute hover:cursor-pointer 
                    ${StyleBanner.positionAnimationCloud}
                    sm:bottom-[-50px] sm:left-[-80px] sm:h-[120px] sm:w-[120px]
                    md:bottom-[-50px] md:left-[-60px] md:h-[120px] md:w-[120px]
                    lg:bottom-[-20px] lg:left-[-90px] lg:h-[160px] lg:w-[160px]
                    xl:bottom-[-40px] xl:left-[-150px] xl:h-[200px] xl:w-[200px]
                    2xl:bottom-[-90px] 2xl:left-[-180px] 2xl:h-[260px] 2xl:w-[260px]
                  `}
                  onClick={() => router.push('/services/cloudserver')}
                >
                  <Image layout="fill" src={ImageHoverCloud} alt="introduce" />
                  <CurvedTextCloud className="curved-text">
                    <svg viewBox={`0 0 ${d + offset * 2} ${r + offset * 2}`}>
                      <path
                        id="curve"
                        d={`M${offset},${r + offset} A${r},${r} 0 0,1 ${
                          d + offset
                        },${r + offset}`}
                      />
                      <text>
                        <textPath
                          xlinkHref="#curve"
                          startOffset="50%"
                          fontSize={14}
                        >
                          {'CLOUD SERVER'}
                        </textPath>
                      </text>
                    </svg>
                  </CurvedTextCloud>
                </div>
                <div
                  className={`absolute ${stylesAnimation.zoomOut}
                    ${StyleBanner.animationCloud}
                    sm:bottom-[30px] sm:left-[0px] sm:h-[60px] sm:w-[60px]
                    md:bottom-[30px] md:left-[20px] md:h-[60px] md:w-[60px]
                    lg:bottom-[60px] lg:left-[5px] lg:h-[80px] lg:w-[80px]
                    xl:bottom-[85px] xl:left-[-10px] xl:h-[100px] xl:w-[100px]
                    2xl:bottom-[70px] 2xl:left-[-10px] 2xl:h-[130px] 2xl:w-[130px]
                  `}
                >
                  <Image layout="fill" src={ImageCloud} alt="introduce" />
                </div>
              </div>
            ) : (
              <div
                className={`absolute hover:cursor-pointer
                  ${StyleBanner.animationCloud}
                  sm:bottom-[30px] sm:left-[0px] sm:h-[60px] sm:w-[60px]
                  md:bottom-[30px] md:left-[20px] md:h-[60px] md:w-[60px]
                  lg:bottom-[60px] lg:left-[5px] lg:h-[80px] lg:w-[80px]
                  xl:bottom-[85px] xl:left-[-10px] xl:h-[100px] xl:w-[100px]
                  2xl:bottom-[70px] 2xl:left-[-10px] 2xl:h-[130px] 2xl:w-[130px]
                `}
                onClick={() => router.push('/services/cloudserver')}
                onMouseOver={() => {
                  setHoverItem(true);
                  setSelectedCloud(true);
                }}
                onMouseOut={() => {
                  setHoverItem(false);
                  setSelectedCloud(false);
                }}
              >
                <Image
                  width={130}
                  height={130}
                  src={ImageCloud}
                  alt="introduce"
                />
                {selectedCloud && (
                  <div
                    className={`absolute hover:cursor-pointer ${stylesAnimation.hover}
                      xm:bottom-[-65px] xm:left-[-30px] xm:h-[70px] xm:w-[70px]
                      sd:bottom-[-50px] sd:left-[-55px] sd:h-[70px] sd:w-[70px]
                      md:bottom-[-70px] md:left-[-75px] md:h-[90px] md:w-[90px]
                      lg:bottom-[-100px] lg:left-[-100px] lg:h-[130px] lg:w-[130px]
                      xl:bottom-[-110px] xl:left-[-110px] xl:h-[140px] xl:w-[140px]
                      2xl:bottom-[-180px] 2xl:left-[-210px] 2xl:h-[220px] 2xl:w-[220px]
                    `}
                    onClick={() => router.push('/services/gameconfig')}
                  >
                    <Image
                      layout="fill"
                      src={ImageHoverCloud}
                      alt="introduce"
                    />
                    <CurvedTextCloudHover className="curved-text">
                      <svg viewBox={`0 0 ${d + offset * 2} ${r + offset * 2}`}>
                        <path
                          id="curve"
                          d={`M${offset},${r + offset} A${r},${r} 0 0,1 ${
                            d + offset
                          },${r + offset}`}
                        />
                        <text>
                          <textPath
                            xlinkHref="#curve"
                            startOffset="50%"
                            fontSize={14}
                          >
                            {'CLOUD SERVER'}
                          </textPath>
                        </text>
                      </svg>
                    </CurvedTextCloudHover>
                  </div>
                )}
              </div>
            )}
          </div>
        </div>
      </div>
      <div
        className={`${StyleBanner.contentBannerFooter} xm:px-[16px] sm:mb-[100px] md:hidden`}
      >
        <div
          className={`${StyleBanner.contentTitle} font-montserrat font-bold text-[#1DE9B6]
          sm:text-[24px] sm:leading-[30px]
        `}
        >
          <p className="font-montserrat">Patience</p>
          <p className="font-montserrat">on the path</p>
        </div>
        <div
          className={`${StyleBanner.contentDescription} text-base leading-6 tracking-[0.2em] text-white
            sm:mt-[20px] sm:text-[14px]
        `}
        >
          <p className="font-montserrat tracking-wider">
            This is a technology company on cross-platform application for
            both outsourcing and production by delivering software applications
            contributing to build the life to be better.
          </p>
        </div>
      </div>
      {/* <div className={`absolute bottom-0 h-[200px] w-full`}>
        <Image
          src={BackgroundTransparent}
          alt="background"
          objectFit="cover"
          layout="fill"
        />
      </div> */}
    </div>
  );
};

export default React.memo(Banner);
