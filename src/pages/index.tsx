import React, { useState } from 'react';
import Seo from '@/components/common/Seo';
import DefaultLayout from '@/components/layouts/DefaultLayout';
import Banner from '@/components/ui/Banner';
import AboutUs from '@/components/ui/AboutUs';
import WhatMakeUsDifferent from '@/components/ui/WhatMakeUsDifferent';
import Script from 'next/script';
import dynamic from 'next/dynamic';

const Home = () => {
  const [isDarkMode, setIsDarkMode] = useState(false);
  const rootClassName = isDarkMode ? 'dark-mode' : 'light-mode';
  const toggleMode = () => {
    setIsDarkMode(!isDarkMode);
  };

  const theHost = process.env.PROJECT_BASE_URL ?? '';

  return (
    <div className={rootClassName}>
      <button onClick={toggleMode}>Toggle Mode</button>
    <React.Fragment>
      <Seo
        title=""
        description={''}
        url={theHost}
        thumbnailUrl=""
      />
      <Script
        src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"
        strategy="afterInteractive"
      />
      <Script id="google-analytics" strategy="afterInteractive">
        {`
          window.dataLayer = window.dataLayer || [];
          function gtag(){window.dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'GA_MEASUREMENT_ID');
        `}
      </Script>
      <Banner />
      <AboutUs />
      <WhatMakeUsDifferent />
    </React.Fragment>
  </div>
   
  );
};

Home.Layout = DefaultLayout;

export default Home;
