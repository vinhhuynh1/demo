import React from 'react'
import LinePage from '@public/images/linePage.png'
import Image from '../../../../components/common/Image';

const TitleService = React.memo(({ title }: { title: string }) => {
 
  return(
    <div>
      <p className="font-montserrat font-bold text-2xl text-[#1DE9B6]">{title}</p>
      <Image src={LinePage} alt="line" />
    </div>
  )
});

TitleService.displayName = 'TitleService'

export default TitleService;